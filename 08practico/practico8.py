import cv2
import numpy as np
import sys


class RectificacionDeImagenes:
    def __init__(self) -> None:

        # se fija en elemento [1] de argv. ese es el nombre del
        # archivo a procesar.el elemento[0] es el nombre del programa
        if len(sys.argv) > 1:
            self.filename = sys.argv[1]  # guarda el nombre del archivo
        else:
            print("Pass a filename as first argument")
            sys.exit(0)  # sale del programa

        self.img = cv2.imread(self.filename, cv2.IMREAD_COLOR)
        self.img_aux = np.copy(self.img)

        self.draw = False
        self.points = []

        cv2.namedWindow("imagen")
        cv2.setMouseCallback("imagen", self.getPoints)

        self.print_menu()

        while True:
            cv2.imshow("imagen", self.img)
            # cv2.imwrite('prueba.jpg', img)
            key = cv2.waitKey(1)
            if key == ord("q"):
                break
            if key == ord("r"):
                self.img = cv2.imread(self.filename)
                self.points = []
            if key == ord("h"):
                if len(self.points) < 4:
                    print("\nDebe seleccionar 4 puntos primero")
                else:
                    self.img = self.img_aux.copy()
                    self.points = self.ordena_puntos()
                    self.img = self.rectificar()
                    cv2.imwrite("output.jpg", self.img)

        cv2.destroyAllWindows()

    def print_menu(self) -> None:

        print("Con el mouse puede seleccione 4 puntos donde rectificar")
        print("Con la letra 'h'se rectificará la imagen con los puntos seleccionados")
        print("Con la letra 'r' se reinicia la seleccion de puntos")
        print("Con la letra 'q' finaliza la ejecución del programa")

    def getPoints(self, event, x, y, flags, param) -> None:

        if event == cv2.EVENT_LBUTTONDOWN:
            if len(self.points) < 4:
                self.points.append([x, y])
            else:
                self.points.clear()
                self.points.append([x, y])
        elif event == cv2.EVENT_LBUTTONUP:
            cv2.circle(
                self.img,
                (x, y),
                5,
                (
                    120,
                    255,
                ),
                -1,
            )
            print(f"Punto seleccionado en cordenadas: ({x},{y})")

    def ordena_puntos(self) -> list:
        mod = []
        # Calcula modulos
        for x, y in self.points:
            mod.append(np.sqrt(x**2 + y**2))

        # Arreglo de puntos ordenados
        new_points = [0, 0, 0, 0]

        # Define puntos A y D
        new_points[0] = self.points.pop(mod.index(min(mod)))
        new_points[3] = self.points.pop(mod.index(max(mod)) - 1)

        # Define puntos B y C
        if self.points[0][1] < self.points[1][1]:
            new_points[1] = self.points[0]
            new_points[2] = self.points[1]
        else:
            new_points[1] = self.points[1]
            new_points[2] = self.points[0]

        return new_points

    def rectificar(self) -> list:

        p_origen = np.float32(self.points)

        # Diferencia de puntos BA, DC, CA y DB
        difBA = p_origen[1] - p_origen[0]
        difDC = p_origen[3] - p_origen[2]
        difCA = p_origen[2] - p_origen[0]
        difDB = p_origen[3] - p_origen[1]

        # Ancho y largo
        wAB = np.sqrt(difBA[0] ** 2 + difBA[1] ** 2)
        wCD = np.sqrt(difDC[0] ** 2 + difDC[1] ** 2)
        hAC = np.sqrt(difCA[0] ** 2 + difCA[1] ** 2)
        hBD = np.sqrt(difDB[0] ** 2 + difDB[1] ** 2)

        # Ancho y largo maximos
        wide = max(int(wAB), int(wCD)) - 1
        height = max(int(hBD), int(hAC)) - 1

        # Puntos destino
        p_destino = np.float32([[0, 0], [wide, 0], [0, height], [wide, height]])
        M = cv2.getPerspectiveTransform(p_origen, p_destino)
        img_salida = cv2.warpPerspective(self.img, M, (wide, height))
        return img_salida


if __name__ == "__main__":
    practico = RectificacionDeImagenes()
